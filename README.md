# HTTP status by David Revoy

This project aims to build a website which will be to [David Revoy](https://www.davidrevoy.com/) what [http.cat](https://http.cat/) is to cats: a collection of images associated to HTTP status.

## The generated site

You can see it on <https://http.framasoft.org/>.

## License

The project is licensed under the terme of the AGPL-v3. See [LICENSE](LICENSE) file for details.

The images from David Revoy are licensed under the terms of the [CC-By license](https://creativecommons.org/licenses/by/4.0/).
