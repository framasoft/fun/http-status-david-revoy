#!/bin/bash

declare -A STATUSES=(
    [100]="Continue"
    [101]="Switching Protocols"
    [102]="Processing"
    [103]="Early Hints"
    [200]="OK"
    [201]="Created"
    [202]="Accepted"
    [203]="Non-Authoritative Information"
    [204]="No Content"
    [205]="Reset Content"
    [206]="Partial Content"
    [207]="Multi-Status"
    [208]="Already Reported"
    [226]="IM Used"
    [300]="Multiple Choices"
    [301]="Moved Permanently"
    [302]="Found"
    [303]="See Other"
    [304]="Not Modified"
    [305]="Use Proxy"
    [306]="Switch Proxy"
    [307]="Temporary Redirect"
    [308]="Permanent Redirect"
    [400]="Bad Request"
    [401]="Unauthorized"
    [402]="Payment Required"
    [403]="Forbidden"
    [404]="Not Found"
    [405]="Method Not Allowed"
    [406]="Not Acceptable"
    [407]="Proxy Authentication Required"
    [408]="Request Timeout"
    [409]="Conflict"
    [410]="Gone"
    [411]="Length Required"
    [412]="Precondition Failed"
    [413]="Payload Too Large"
    [414]="URI Too Long"
    [415]="Unsupported Media Type"
    [416]="Range Not Satisfiable"
    [417]="Expectation Failed"
    [418]="I’m a teapot"
    [421]="Misdirected Request"
    [422]="Unprocessable Entity"
    [423]="Locked"
    [424]="Failed Dependency"
    [425]="Too Early"
    [426]="Upgrade Required"
    [428]="Precondition Required"
    [429]="Too Many Requests"
    [431]="Request Header Fields Too Large"
    [451]="Unavailable For Legal Reasons"
    [500]="Internal Server Error"
    [501]="Not Implemented"
    [502]="Bad Gateway"
    [503]="Service Unavailable"
    [504]="Gateway Timeout"
    [505]="HTTP Version Not Supported"
    [506]="Variant Also Negotiates"
    [507]="Insufficient Storage"
    [508]="Loop Detected"
    [510]="Not Extended"
    [511]="Network Authentication Required"
)

rm -rf public/
mkdir public

cat <<EOF > public/index.html
<!DOCTYPE html>
<html lang="fr">

<head>
  <title>David Revoy’s HTTP status</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width,initial-scale=1">
  <meta content="David Revoy’s HTTP status" name="description">
  <meta content="David Revoy" name="author">
  <meta content="David Revoy’s HTTP status" property="og:title">
  <meta content="en" property="og:locale">
  <meta content="website" property="og:type">
  <meta content="https://http.framasoft.org/" property="og:url">
  <meta content="https://http.framasoft.org/img/favicon.png" property="og:image">
  <meta content="Images from David Revoy for HTTP status" property="og:description">
  <link rel="icon" type="image/png" href="/img/favicon.png">
  <style>
    body {
      font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, sans-serif;
      color: rgb(51, 51, 51);
      margin: 0;
    }

    .container {
      background-color: #ffffffd9;
      margin: 10px;
      max-width: 1665px;
      margin: 15px auto 15px auto;
      padding: 15px;
      border-radius: 15px;
    }

    a {
      color: #0C5B7A;
    }

    div.status {
        display: flex;
        flex-wrap: wrap;
    }

    div.gallery {
      margin: 5px;
      border: 1px solid #ccc;
      float: left;
      width: 555px;
      flex: 1;
      flex-basis: 540px;
    }

    div.gallery:hover {
      border: 1px solid #777;
    }

    div.gallery img {
      width: 100%;
      height: auto;
    }

    div.desc {
      padding: 15px;
      text-align: center;
    }

    footer {
        text-align: center;
    }
    @media (prefers-color-scheme: dark) {
        html, body, .container {
            background-color: #181a1b !important;
            border-color: #736b5e;
            color: #e8e6e3;
        }
        a {
            color: rgb(123, 209, 242);
        }
    }
  </style>
</head>

<body>
  <main class="container">
    <h1>David Revoy’s HTTP status</h1>
    <p>HTTP status pages using images from <a href="https://www.davidrevoy.com/">David Revoy</a>.<br/>
       Those images are licensed under the terms of the CC-By license.<br />
       You can support David on <a href="https://www.patreon.com/join/davidrevoy?">Patreon</a>, <a href="https://liberapay.com/davidrevoy/">Liberapay</a>, <a href="https://fr.tipeee.com/pepper-carrot">Tipeee</a>, <a href="https://paypal.me/davidrevoy">Paypal</a> (see <a href="https://www.peppercarrot.com/en/support/index.html">here for other possibilities</a>).</p>
    <h2>Usage</h2>
    <p>See status page:</p>
    <pre>https://http.framasoft.org/[status_code]</pre>
    <details>
        <summary>see more</summary>
        <p>Download status page:</p>
        <pre>wget https://http.framasoft.org/[status_code] -O [status_code].html</pre>
        <p>Use it in <a href="https://nginx.org/en/docs/http/ngx_http_core_module.html#error_page">nginx</a>:</p>
        <pre>error_page [status_code] /[status_code].html;</pre>
        <p>Use it in <a href="https://httpd.apache.org/docs/2.4/mod/core.html#errordocument">Apache</a>:</p>
        <pre>ErrorDocument [status_code] /[status_code].html</pre>
    </details>
    <h2>Available status</h2>
    <div class="status">
EOF

cp -a img public/

for i in img/status/*
do
    STATUS=$(basename $i .jpg)
    DESC=${STATUSES[$STATUS]}
    mkdir public/$STATUS
    cat << EOF >> public/index.html
        <div class="gallery">
            <a href="$STATUS">
                <img src="$i" width="600" height="400">
                <div class="desc">$STATUS $DESC</div>
            </a>
        </div>
EOF
    cat <<EOF > public/$STATUS/index.html
<!DOCTYPE html>
<html lang="en">

<head>
  <title>HTTP status $STATUS</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width,initial-scale=1">
  <meta content="$DESC" name="description">
  <meta content="David Revoy" name="author">
  <meta content="$DESC" property="og:title">
  <meta content="en" property="og:locale">
  <meta content="website" property="og:type">
  <meta content="https://http.framasoft.org/$STATUS/" property="og:url">
  <meta content="https://http.framasoft.org/$i" property="og:image">
  <meta content="Image from David Revoy for HTTP status $STATUS" property="og:description">
  <link rel="icon" type="image/png" href="/img/favicon.png">
  <style>
    body {
      font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, sans-serif;
      background-image: url('../$i');
      background-size: cover;
      background-size: initial;
      background-position: top;
      color: rgb(51, 51, 51);
      text-align: center;
      margin: 0;
    }

    .container {
      background-color: #ffffffd9;
      margin: 10px;
      max-width: 600px;
      margin: 15px auto 15px auto;
      padding: 15px;
      border-radius: 15px;
    }

    .container h1 {
      font: 20px/28px -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, sans-serif;
      font-weight: 400;
    }

    a {
      color: #0C5B7A;
    }
    @media (prefers-color-scheme: dark) {
        .container {
            background-color: #323537 !important;
            color: #e8e6e3;
        }
        a {
            color: rgb(123, 209, 242);
        }
    }
  </style>
</head>

<body>
  <main class="container">
    <h1>$STATUS $DESC</h1>
    <hr>
    <small>The image on this page is a creation of <a href="https://www.davidrevoy.com/">David Revoy</a> under the CC-By license.</small>
  </main>
</body>

</html>
EOF
done

    cat <<EOF >> public/index.html
    </div>
    <hr>
    <footer>
        <small>Site created by <a href="https://framasoft.org">Framasoft</a>, sources available on <a href="https://framagit.org/framasoft/fun/http-status-david-revoy">Framagit</a></small>
    </footer>
  </main>
</body>

</html>
EOF
